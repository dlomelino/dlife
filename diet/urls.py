from django.conf.urls import url
from diet import views

urlpatterns = [
    url(r'^$', views.daily, name='diet_home'),
    url(r'date/(?P<month>\d+)/(?P<day>\d+)/(?P<year>\d+)', views.daily, name='diet_home_date'),
    url(r'refresh/(?P<month>\d+)/(?P<day>\d+)/(?P<year>\d+)', views.refresh, name='diet_refresh'),
]