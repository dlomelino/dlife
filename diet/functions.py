import base64
import urllib2
import urllib
import json
import cherrypy
import webbrowser
from diet.models import *
from settings import *
import fitbit
from fitbit.exceptions import HTTPUnauthorized, HTTPTooManyRequests
import sys
import threading
import traceback
from fitbit.api import FitbitOauth2Client
from oauthlib.oauth2.rfc6749.errors import MismatchingStateError, MissingTokenError


def fitbit_init():
    access_token = Tokens.objects.get(name='Fitbit Access').value
    refresh_token = Tokens.objects.get(name='Fitbit Refresh').value

    try:
        # Create a fitbit client with the tokens from the database
        client = fitbit_create_client(access_token, refresh_token)

        # Perform an API call to see if we're authorized
        client.get_bodyweight(period='1d')

        return client
    except HTTPUnauthorized as e:
        access_token, refresh_token = fitbit_refresh_tokens(refresh_token)

        try:
            # Create a fitbit client with the newly generated tokens
            client = fitbit_create_client(access_token, refresh_token)

            # Perform an API call to see if we're authorized
            client.get_bodyweight(period='1d')

            return client
        except HTTPUnauthorized as e:
            # We've tried once. We are done with this request.
            return 'exception', e.message
        except HTTPTooManyRequests as e:
            return 'exception', e.message
    except HTTPTooManyRequests as e:
        return 'exception', e.message


def fitbit_create_client(access_token, refresh_token):
    client = fitbit.Fitbit(
        OAUTH2_CLIENT_ID,
        CLIENT_SECRET,
        access_token=access_token,
        refresh_token=refresh_token
    )

    return client


def fitbit_refresh_tokens(refresh_token):
    print 'Refreshing tokens...'
    body = {
        'grant_type': 'refresh_token',
        'refresh_token': refresh_token,
    }
    body_encoded = urllib.urlencode(body)

    tokenreq = urllib2.Request(REFRESH_URI, body_encoded)
    tokenreq.add_header('Authorization', 'Basic ' + base64.b64encode(OAUTH2_CLIENT_ID + ":" + CLIENT_SECRET))
    tokenreq.add_header('Content-Type', 'application/x-www-form-urlencoded')

    try:
        tokenresponse = urllib2.urlopen(tokenreq)
        response = tokenresponse.read()
        response_json = json.loads(response)
        if update_tokens(response_json['access_token'], response_json['refresh_token']):
            print 'Success refreshing tokens.'

            return response_json['access_token'], response_json['refresh_token']

        return False, False
    except urllib2.URLError as e:
        if 'Refresh token invalid' in e.read():
            try:
                access_token, refresh_token = refresh_tokens()

                if update_tokens(access_token, refresh_token):
                    print 'Success refreshing tokens.'

                    return access_token, refresh_token

                return False, False

            except Exception as e:
                print e.message

                return False, False

        return False, False


def update_tokens(access_token, refresh_token):
    try:
        t = Tokens.objects.get(name='Fitbit Access')
        t.value = access_token
        t.save()
        t = Tokens.objects.get(name='Fitbit Refresh')
        t.value = refresh_token
        t.save()

        return True
    except Exception as e:
        print e.message

        return False


def calculate_maintenance_calories(weight):
    maintenance = int(round(weight * 17, 2))

    return maintenance


def calculate_daily_calories(weight):
    maintenance = calculate_maintenance_calories(weight)
    daily_calories = int(round(maintenance + CALORIES_SURPLUS_VERY_ACTIVE + CALORIES_SURPLUS_GAIN))

    return daily_calories


def calculate_daily_carbs(calories, protein, fat):
    total_carbs_calories = int(round(calories - (protein['calories'] + fat['calories']), 2))
    total_carbs_grams = int(round(total_carbs_calories / CALORIES_PER_GRAM_CARBS, 2))

    return {'calories': total_carbs_calories, 'grams': total_carbs_grams}


def calculate_daily_protein(weight):
    total_protein_grams = int(round(weight * 1.5, 2))
    total_protein_calories = int(round(total_protein_grams * CALORIES_PER_GRAM_PROTEIN, 2))

    return {'calories': total_protein_calories, 'grams': total_protein_grams}


def calculate_daily_fat(calories):
    total_fat_calories = int(round(calories * 0.20, 2))
    total_fat_grams = int(round(total_fat_calories / CALORIES_PER_GRAM_FAT, 2))

    return {'calories': total_fat_calories, 'grams': total_fat_grams}


def percent_in_range(nutrient, percent):
    """
    See if the percentage of the nutrient passed in is within
    a given satisfactory range of total calorie percentage.
    """
    in_range = 0

    if (MY_CALORIES_PERCENT[nutrient] - MY_CALORIES_PERCENT['range']) <= percent <= \
            (MY_CALORIES_PERCENT[nutrient] + MY_CALORIES_PERCENT['range']):
            in_range = 1
    elif percent > (MY_CALORIES_PERCENT[nutrient] + MY_CALORIES_PERCENT['range']):
        in_range = 2

    return in_range


class OAuth2Server:
    def __init__(self, client_id, client_secret,
                 redirect_uri):
        """ Initialize the FitbitOauth2Client """
        self.redirect_uri = redirect_uri
        self.success_html = """
            <h1>You are now authorized to access the Fitbit API!</h1>
            <br/><h3>You can close this window</h3>"""
        self.failure_html = """
            <h1>ERROR: %s</h1><br/><h3>You can close this window</h3>%s"""
        self.oauth = FitbitOauth2Client(client_id, client_secret)

    def browser_authorize(self):
        """
        Open a browser to the authorization url and spool up a CherryPy
        server to accept the response
        """
        url, _ = self.oauth.authorize_token_url(redirect_uri=self.redirect_uri)
        # Open the web browser in a new thread for command-line browser support
        threading.Timer(1, webbrowser.open, args=(url,)).start()
        cherrypy.quickstart(self)

    @cherrypy.expose
    def index(self, state, code=None, error=None):
        """
        Receive a Fitbit response containing a verification code. Use the code
        to fetch the access_token.
        """
        error = None
        if code:
            try:
                self.oauth.fetch_access_token(code, self.redirect_uri)
            except MissingTokenError:
                error = self._fmt_failure(
                    'Missing access token parameter.</br>Please check that '
                    'you are using the correct client_secret')
            except MismatchingStateError:
                error = self._fmt_failure('CSRF Warning! Mismatching state')
        else:
            error = self._fmt_failure('Unknown error while authenticating')
        # Use a thread to shutdown cherrypy so we can return HTML first
        self._shutdown_cherrypy()
        return error if error else self.success_html

    def _fmt_failure(self, message):
        tb = traceback.format_tb(sys.exc_info()[2])
        tb_html = '<pre>%s</pre>' % ('\n'.join(tb)) if tb else ''
        return self.failure_html % (message, tb_html)

    def _shutdown_cherrypy(self):
        """ Shutdown cherrypy in one second, if it's running """
        if cherrypy.engine.state == cherrypy.engine.states.STARTED:
            threading.Timer(1, cherrypy.engine.exit).start()


def refresh_tokens():
    server = OAuth2Server(
        client_id=OAUTH2_CLIENT_ID,
        client_secret=CLIENT_SECRET,
        redirect_uri=CALLBACK_URI,
    )

    server.browser_authorize()

    return server.oauth.token['access_token'], server.oauth.token['refresh_token']
