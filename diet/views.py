from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from datetime import datetime, timedelta
import math
from functions import *
from diet.models import Body, Macros


def refresh(request, month=None, day=None, year=None):
    client = fitbit_init()

    if type(client) is tuple and client[0] and client[0] == 'exception':
        return render(request, 'diet/error.html', {'message': client[1]})

    today = datetime.today().date()

    if not month or not day or not year:
        use_date = today
    else:
        date_str = month + '/' + day + '/' + year
        use_date = datetime.strptime(date_str, '%m/%d/%Y')

    # Get body details from Fitbit API
    body = get_body_details(use_date, client)
    if body:
        weight = body['weight']
        body_fat = body['fat']
        bmi = body['bmi']

        # Check if an entry exists in the database for this date
        exists = Body.objects.filter(date=use_date)
        if exists:
            # Update it
            exists.update(
                weight=weight,
                body_fat=body_fat,
                bmi=bmi,
            )
        else:
            # Insert it
            post = Body.objects.create(
                date=use_date,
                weight=weight,
                body_fat=body_fat,
                bmi=bmi,
            )

    foods = get_foods_summary(use_date, client)
    if foods:
        calories = foods['calories']
        carbs = foods['carbs']['grams']
        protein = foods['protein']['grams']
        fat = foods['fat']['grams']
        sodium = foods['sodium']
        fiber = foods['fiber']
        water = foods['water']

        exists = Macros.objects.filter(date=use_date)
        if exists:
            # Update it
            exists.update(
                calories=calories,
                carbs=carbs,
                protein=protein,
                fat=fat,
                sodium=sodium,
                fiber=fiber,
                water=water,
            )
        else:
            # Insert it
            post = Macros.objects.create(
                date=use_date,
                calories=calories,
                carbs=carbs,
                protein=protein,
                fat=fat,
                sodium=sodium,
                fiber=fiber,
                water=water,
            )

    water_goal = get_water_goal(client)
    print foods
    print water_goal

    return HttpResponseRedirect(reverse('diet_home_date', kwargs={'month': month, 'day': day, 'year': year}))


def daily(request, month=None, day=None, year=None):
    today = datetime.today().date()

    if not month or not day or not year:
        use_date = today
    else:
        date_str = month + '/' + day + '/' + year
        use_date = datetime.strptime(date_str, '%m/%d/%Y')

    date_string = use_date.strftime('%A, %B %-d, %Y')
    date_this = {
        'month': use_date.month,
        'day': use_date.day,
        'year': use_date.year
    }
    day_today = {
        'month': today.month,
        'day': today.day,
        'year': today.year,
    }
    day_before = {
        'month': (use_date - timedelta(1)).month,
        'day': (use_date - timedelta(1)).day,
        'year': (use_date - timedelta(1)).year,
    }
    day_after = {
        'month': (use_date + timedelta(1)).strftime('%m'),
        'day': (use_date + timedelta(1)).strftime('%d'),
        'year': (use_date + timedelta(1)).strftime('%Y'),
    }

    body = get_body_details(use_date)
    maintenance = calculate_maintenance_calories(body['weight'])
    foods = get_foods_summary(use_date)
    # water_goal = get_water_goal(client)  # Get it from Fitbit
    water_goal = WATER_GOAL

    calories = calculate_daily_calories(body['weight'])  # calculated by body weight
    # calories = CALORIES_TOTAL  # calculated by gain surplus and activity level
    protein = calculate_daily_protein(body['weight'])
    fat = calculate_daily_fat(calories)
    carbs = calculate_daily_carbs(calories, protein, fat)

    targets = {
        'calories': calories,
        'protein': protein,
        'fat': fat,
        'carbs': carbs,
        'sodium': RECOMMENDED_DAILY_SODIUM_MAX,
        'fiber': RECOMMENDED_DAILY_FIBER,
        'water': water_goal,
    }

    recommended = {
        'carbs': {
            'percent': MY_CALORIES_PERCENT['carbs'],
        },
        'protein': {
            'percent': MY_CALORIES_PERCENT['protein'],
        },
        'fat': {
            'percent': MY_CALORIES_PERCENT['fat'],
        },
        'sodium': {
            'min': RECOMMENDED_DAILY_SODIUM_MIN,
            'max': RECOMMENDED_DAILY_SODIUM_MAX,
        },
        'fiber': RECOMMENDED_DAILY_FIBER,
        'potassium': RECOMMENDED_DAILY_POTASSIUM,
    }

    percents = {
        'calories': 0,
        'carbs': {
            'value': 0,
            'in_range': 0,
        },
        'protein': {
            'value': 0,
            'in_range': 0,
        },
        'fat': {
            'value': 0,
            'in_range': 0,
        },
        'sodium': 0,
        'fiber': 0,
        'water': 0
    }
    if foods and foods['calories']:
        percents['calories'] = int(round((float(foods['calories']) / float(targets['calories'])) * 100, 2))
        percents['carbs']['value'] = int(round((float(foods['carbs']['calories']) / float(foods['calories'])) * 100, 2))
        percents['carbs']['in_range'] = percent_in_range('carbs', percents['carbs']['value'])
        percents['protein']['value'] = int(round((float(foods['protein']['calories']) / float(foods['calories'])) * 100, 2))
        percents['protein']['in_range'] = percent_in_range('protein', percents['protein']['value'])
        percents['fat']['value'] = int(round((float(foods['fat']['calories']) / float(foods['calories'])) * 100, 2))
        percents['fat']['in_range'] = percent_in_range('fat', percents['fat']['value'])
        percents['sodium'] = int(round((float(foods['sodium']) / targets['sodium']) * 100, 2))
        percents['fiber'] = int(round((float(foods['fiber']) / targets['fiber']) * 100, 2))
    if foods and foods['water']:
        percents['water'] = int(round((float(foods['water']) / targets['water']) * 100, 2))

    diffs = {
        'calories': {},
        'carbs': {
            'calories': {},
            'grams': {},
        },
        'protein': {
            'calories': {},
            'grams': {},
        },
        'fat': {
            'calories': {},
            'grams': {},
        },
    }
    if foods:
        diffs['calories']['under'] = targets['calories'] - foods['calories']
        diffs['calories']['over'] = foods['calories'] - targets['calories']
        diffs['carbs']['calories']['under'] = targets['carbs']['calories'] - foods['carbs']['calories']
        diffs['carbs']['calories']['over'] = foods['carbs']['calories'] - targets['carbs']['calories']
        diffs['carbs']['grams']['under'] = targets['carbs']['grams'] - foods['carbs']['grams']
        diffs['carbs']['grams']['over'] = foods['carbs']['grams'] - targets['carbs']['grams']
        diffs['protein']['calories']['under'] = targets['protein']['calories'] - foods['protein']['calories']
        diffs['protein']['calories']['over'] = foods['protein']['calories'] - targets['protein']['calories']
        diffs['protein']['grams']['under'] = targets['protein']['grams'] - foods['protein']['grams']
        diffs['protein']['grams']['over'] = foods['protein']['grams'] - targets['protein']['grams']
        diffs['fat']['calories']['under'] = targets['fat']['calories'] - foods['fat']['calories']
        diffs['fat']['calories']['over'] = foods['fat']['calories'] - targets['fat']['calories']
        diffs['fat']['grams']['under'] = targets['fat']['grams'] - foods['fat']['grams']
        diffs['fat']['grams']['over'] = foods['fat']['grams'] - targets['fat']['grams']

    context_dict = {
        'body': body,
        'maintenance': maintenance,
        'targets': targets,
        'recommended': recommended,
        'percents': percents,
        'foods': foods,
        'diffs': diffs,
        'date_string': date_string,
        'date_this': date_this,
        'day_today': day_today,
        'day_before': day_before,
        'day_after': day_after,
    }

    return render(request, 'diet/index.html', context_dict)


def get_body_details(date, client=None):
    weight = 0
    fat = 0
    bmi = 0

    if client:
        bodyweight = client.get_bodyweight(base_date=date)
        if bodyweight['weight']:
            weight = bodyweight['weight'][0]['weight']
            fat = round(bodyweight['weight'][0]['fat'], 2)
            bmi = bodyweight['weight'][0]['bmi']
    else:
        try:
            body = Body.objects.get(date=date)
            if body:
                weight = body.weight
                fat = round(body.body_fat, 2)
                bmi = body.bmi
        except ObjectDoesNotExist as e:
            print 'Body does not exist for date %s' % str(date)

    return {
        'weight': weight,
        'fat': fat,
        'bmi': bmi,
    }


def get_foods_summary(date, client=None):
    foods = {}

    if client:
        foods = client._COLLECTION_RESOURCE('foods/log', date=date)
    else:
        try:
            foods_date = Macros.objects.get(date=date)
            foods = {
                'summary': {
                    'calories': foods_date.calories,
                    'carbs': foods_date.carbs,
                    'protein': foods_date.protein,
                    'fat': foods_date.fat,
                    'sodium': foods_date.sodium,
                    'fiber': foods_date.fiber,
                    'water': foods_date.water,
                }
            }
        except ObjectDoesNotExist as e:
            print 'Macros does not exist for date %s' % str(date)

    if foods:
        foods_summary = foods['summary']

        foods_summary = {
            'calories': foods_summary['calories'],
            'carbs': {
                'calories': int(round(foods_summary['carbs'], 2)) * CALORIES_PER_GRAM_CARBS,
                'grams': int(round(foods_summary['carbs'], 2)),
            },
            'protein': {
                'calories': int(round(foods_summary['protein'], 2)) * CALORIES_PER_GRAM_PROTEIN,
                'grams': int(round(foods_summary['protein'], 2)),
            },
            'fat': {
                'calories': int(round(foods_summary['fat'], 2)) * CALORIES_PER_GRAM_FAT,
                'grams': int(round(foods_summary['fat'], 2)),
            },
            'sodium': int(round(foods_summary['sodium'], 2)),
            'fiber': int(round(foods_summary['fiber'], 2)),
            'water': round(foods_summary['water'], 1),
        }

        return foods_summary

    return False


def get_water_goal(client=None):
    water_goal = math.ceil(client.water_goal()['goal']['goal'])

    return water_goal
