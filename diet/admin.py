from django.contrib import admin
from diet.models import *


class TokensAdmin(admin.ModelAdmin):
    list_display = ['name', 'value']

admin.site.register(Tokens, TokensAdmin)
admin.site.register(Body)
admin.site.register(Macros)
