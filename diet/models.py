from django.db import models


class Tokens(models.Model):
    name = models.CharField(max_length=256, blank=False, default='')
    value = models.TextField()

    def __unicode__(self):
        return self.name


class Body(models.Model):
    date = models.DateField()
    weight = models.FloatField()    # Pounds
    body_fat = models.FloatField()  # Percentage
    bmi = models.FloatField()       # Float


class Macros(models.Model):
    date = models.DateField()
    calories = models.IntegerField()
    carbs = models.IntegerField()       # grams
    protein = models.IntegerField()     # grams
    fat = models.IntegerField()         # grams
    sodium = models.IntegerField()      # milligrams
    fiber = models.IntegerField()       # grams
    water = models.FloatField()         # oz
