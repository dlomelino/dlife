from __future__ import unicode_literals

from django.db import models

muscle_choices = (
    ('chest', 'Chest'),
    ('back', 'Back'),
    ('shoulders', 'Shoulders'),
    ('biceps', 'Biceps'),
    ('triceps', 'Triceps'),
    ('legs', 'Legs'),
    ('forearms', 'Forearms'),
    ('abs', 'Abs'),
    ('cardio', 'Cardio'),
    ('stretch', 'Stretch'),
)

workout_locations = (
    ('home', 'Home'),
    ('nifs', 'NIFS'),
    ('pf', 'Planet Fitness'),
    ('popfc', 'Precedent Office Park Fitness Center')
)


class Exercise(models.Model):
    name = models.CharField(max_length=256, blank=False)
    muscle = models.CharField(max_length=100, choices=muscle_choices, default='')
    notes = models.TextField(blank=True, default='')

    def __unicode__(self):
        return self.name


class Workout(models.Model):
    location = models.CharField(max_length=256, choices=workout_locations, default='nifs')
    time_started = models.DateTimeField()
    time_ended = models.DateTimeField()
    notes = models.TextField(blank=True)

    def __unicode__(self):
        return self.time_started.strftime("%A, %b %-d, %Y") + ' @ ' + self.location.upper()


class WorkoutSets(models.Model):
    workout = models.ForeignKey(Workout, on_delete=models.CASCADE)
    exercise = models.ForeignKey(Exercise)
    weight = models.IntegerField(blank=True, default=0)
    reps = models.IntegerField(blank=False)
    superset = models.ForeignKey('self', blank=True, null=True)
    failure = models.BooleanField(default=False)
    seq = models.IntegerField(blank=True, default=0)

    class Meta:
        ordering = ['-workout__id']

    def __unicode__(self):
        return self.workout.time_started.strftime("%A, %b %-d, %Y") \
               + ': '\
               + self.exercise.muscle + ': '\
               + self.exercise.name\
               + ' - '\
               + str(self.weight) + 'lbs '\
               + str(self.reps) + ' reps'
