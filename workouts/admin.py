from django.contrib import admin
from workouts.models import *


admin.site.register(Exercise)
admin.site.register(Workout)
admin.site.register(WorkoutSets)
