from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
import datetime
from functions import *
from models import *
from forms import WorkoutForm, WorkoutSetsForm
from workouts.chart_ytd_sets import highcharts_json


def calendar(request, month=None, year=None):
    my_year = int(year) if year else datetime.datetime.now().year
    my_month = int(month) if month else datetime.datetime.now().month
    my_previous_year = my_year
    my_previous_month = my_month - 1
    if my_previous_month == 0:
        my_previous_year = my_year - 1
        my_previous_month = 12
    my_next_year = my_year
    my_next_month = my_month + 1
    if my_next_month == 13:
        my_next_year = my_year + 1
        my_next_month = 1
    my_year_after_this = my_year + 1
    my_year_before_this = my_year - 1

    my_calendar_from_month = datetime.datetime(my_year, my_month, 1)
    my_calendar_to_month = datetime.datetime(my_next_year, my_next_month, 1)

    my_workouts = Workout.objects\
        .filter(time_started__gte=my_calendar_from_month)\
        .filter(time_ended__lt=my_calendar_to_month)

    context = {
        'workouts': my_workouts,
        'month': my_month,
        'month_name': named_month(my_month),
        'year': my_year,
        'previous_month': my_previous_month,
        'previous_month_name': named_month(my_previous_month),
        'previous_year': my_previous_year,
        'next_month': my_next_month,
        'next_month_name': named_month(my_next_month),
        'next_year': my_next_year,
        'year_before_this': my_year_before_this,
        'year_after_this': my_year_after_this,
    }

    return render(request, 'workouts/index.html', context)


def reports(request):
    workout_objs = Workout.objects.all().order_by('-time_started')
    workout_sets = WorkoutSets.objects.all().order_by('-workout__time_started')

    workouts = {}
    for obj in workout_objs:
        month = obj.time_started.month
        day = obj.time_started.day
        year = obj.time_started.year
        month_name = named_month(month)

        if year not in workouts:
            workouts[year] = {}
        if month_name not in workouts[year]:
            workouts[year][month_name] = {
                'num_sets': 0,
            }
        if day not in workouts[year][month_name]:
            workouts[year][month_name][day] = {}

        workouts[year][month_name][day]['workout'] = obj
        workouts[year][month_name][day]['sets'] = [s for s in workout_sets.filter(workout=obj)]
        workouts[year][month_name]['num_sets'] += len(workouts[year][month_name][day]['sets'])

    context = {
        'workouts': workouts,
    }

    return render(request, 'workouts/reports.html', context)


def reports_ytd(request):
    now = datetime.datetime.now()
    this_year = now.year
    ytd_sets = WorkoutSets.objects.filter(workout__time_started__year=this_year)
    ytd_num_sets = 0
    ytds = {}
    for ytd_set in ytd_sets:
        ytd_num_sets += 1
        month = named_month(ytd_set.workout.time_started.month)
        year = ytd_set.workout.time_started.year

        if year not in ytds:
            ytds[year] = {}
        if month not in ytds[year]:
            ytds[year][month] = []

        ytds[year][month].append(ytd_set)

    highcharts_json['title']['text'] = 'Workout Sets for %s' % this_year
    highcharts_json['tooltip']['pointFormat'] = 'Number of sets: <b>{point.y:.0f}</b>'
    for ytd_month in ytds[this_year]:
        num_sets = len(ytds[this_year][ytd_month])
        highcharts_json['series'][0]['data'].append(
            [ytd_month, num_sets],
        )

    context = {
        'ytd_sets': ytd_num_sets,
        'ytd_months': ytds,
        'highcharts_json': highcharts_json,
    }

    return render(request, 'workouts/reports_ytd.html', context)


def reports_exercises(request):
    now = datetime.datetime.now()
    this_year = now.year
    workout_sets = WorkoutSets.objects.filter(workout__time_started__year=this_year)
    exercises = {}
    for workout_set in workout_sets:
        month = named_month(workout_set.workout.time_started.month)
        year = workout_set.workout.time_started.year
        exercise = workout_set.exercise.name

        if year not in exercises:
            exercises[year] = {}
        if month not in exercises[year]:
            exercises[year][month] = {}
        if exercise not in exercises[year][month]:
            exercises[year][month][exercise] = 0

        exercises[year][month][exercise] += 1

    print exercises
    highcharts_json['title']['text'] = 'Exercises for %s' % this_year
    highcharts_json['tooltip']['pointFormat'] = 'Number of sets: <b>{point.y:.0f}</b>'
    for exercise_month in exercises[this_year]:
        num_sets = 0
        for exercise in exercises[this_year][exercise_month]:
            num_sets = exercises[this_year][exercise_month][exercise]

        highcharts_json['series'][0]['data'].append(
            [exercise_month, num_sets],
        )

    context = {
        'num_sets': num_sets,
        'exercises': exercises,
        'highcharts_json': highcharts_json,
    }

    return render(request, 'workouts/reports_exercises.html', context)


def this_month(request):
    today = datetime.datetime.now()

    return calendar(request, today.year, today.month)


def workout_details(request, workout_id):
    workout = Workout.objects.get(id=workout_id)
    workout_sets = WorkoutSets.objects.filter(workout_id=workout_id)

    context = {
        'workout': workout,
        'workout_sets': workout_sets,
    }

    return render(request, 'workouts/workout.html', context)


def form_workout_add(request, month, day, year):
    workout_date = datetime.datetime(int(year), int(month), int(day), 0, 0, 0)

    if request.method == 'GET':
        form = WorkoutForm()
    else:
        form = WorkoutForm(request.POST)

        if form.is_valid():
            location = form.cleaned_data['location']
            workout_date = workout_date.strftime('%Y-%m-%d')
            time_started = workout_date + ' ' + str(form.cleaned_data['time_started'])
            time_ended = workout_date + ' ' + str(form.cleaned_data['time_ended'])
            notes = form.cleaned_data['notes']
            post = Workout.objects.create(
                location=location,
                time_started=time_started,
                time_ended=time_ended,
                notes=notes,
            )

            return HttpResponseRedirect(reverse('workouts_home'))

    workout_date_string = workout_date.strftime('%A, %b. %-d, %Y')

    context = {
        'form': form,
        'action': 'Add',
        'workout_date_string': workout_date_string,
    }

    return render(request, 'workouts/add.html', context)


def form_workout_edit(request, month, day, year):
    workout_date = datetime.datetime(int(year), int(month), int(day), 0, 0, 0)
    try:
        workout = Workout.objects.get(time_started__month=int(month), time_started__day=int(day), time_started__year=int(year))
    except ObjectDoesNotExist as e:
        print 'Workout does not exist'

        return HttpResponseRedirect(reverse('workouts_home'))

    if request.method == 'GET':
        data = {
            'location': workout.location,
            'time_started': workout.time_started.time(),
            'time_ended': workout.time_ended.time(),
            'notes': workout.notes,
        }

        form = WorkoutForm(data)
    else:
        form = WorkoutForm(request.POST)

        if form.is_valid():
            workout.location = form.cleaned_data['location']
            workout_date = workout_date.strftime('%Y-%m-%d')
            workout.time_started = workout_date + ' ' + str(form.cleaned_data['time_started'])
            workout.time_ended = workout_date + ' ' + str(form.cleaned_data['time_ended'])
            workout.notes = form.cleaned_data['notes']
            workout.save()

            return HttpResponseRedirect(reverse('workouts_home'))

    workout_date_string = workout_date.strftime('%A, %b. %-d, %Y')

    context = {
        'form': form,
        'action': 'Edit',
        'workout_id': workout.id,
        'workout_date_string': workout_date_string,
    }

    return render(request, 'workouts/add.html', context)


def form_workout_set_add(request, workout_id):
    workout = Workout.objects.get(id=workout_id)

    if request.method == 'GET':
        form = WorkoutSetsForm()
    else:
        form = WorkoutSetsForm(request.POST)

        if form.is_valid():
            exercise = form.cleaned_data['exercise']
            weight = form.cleaned_data['weight']
            reps = form.cleaned_data['reps']
            sets = form.cleaned_data['sets']
            superset = form.cleaned_data['superset']
            failure = form.cleaned_data['failure']

            post_ids = []
            for num in range(1, sets+1):
                post = WorkoutSets.objects.create(
                    workout=workout,
                    exercise=exercise,
                    weight=weight,
                    reps=reps,
                    superset=superset,
                    failure=failure,
                )

                post_ids.append(post.id)

        return HttpResponseRedirect(reverse('workouts_set_add', kwargs={'workout_id': workout.id}))

    context = {
        'form': form,
        'workout': workout,
    }

    return render(request, 'workouts/add_set.html', context)
