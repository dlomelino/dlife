from django import forms
from django.contrib.admin.widgets import AdminTimeWidget
from workouts.models import workout_locations
from workouts.models import *


class WorkoutForm(forms.Form):
    location = forms.CharField(max_length=256, widget=forms.Select(choices=workout_locations), initial='nifs')
    time_started = forms.TimeField(widget=AdminTimeWidget)
    time_ended = forms.TimeField(widget=AdminTimeWidget)
    notes = forms.CharField(widget=forms.Textarea, required=False)


class WorkoutSetsForm(forms.ModelForm):
    sets = forms.IntegerField(initial=1)

    class Meta:
        model = WorkoutSets
        fields = ['exercise', 'weight', 'sets', 'reps', 'superset', 'failure']
