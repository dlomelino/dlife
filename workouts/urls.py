from django.conf.urls import url
from workouts import views

urlpatterns = [
    url(r'^$', views.calendar, name='workouts_home'),
    url(r'date/(?P<month>\d+)/(?P<year>\d+)', views.calendar, name='workouts_home_date'),
    url(r'add/(?P<month>\d+)/(?P<day>\d+)/(?P<year>\d+)', views.form_workout_add, name='workouts_add'),
    url(r'edit/(?P<month>\d+)/(?P<day>\d+)/(?P<year>\d+)', views.form_workout_edit, name='workouts_edit'),
    url(r'set/add/(?P<workout_id>\d+)', views.form_workout_set_add, name='workouts_set_add'),
    url(r'(?P<workout_id>\d+)', views.workout_details, name='workouts_details'),
    url(r'reports$', views.reports, name='workouts_reports'),
    url(r'reports/ytd', views.reports_ytd, name='workouts_reports_ytd'),
    url(r'reports/exercises', views.reports_exercises, name='workouts_reports_exercises'),
]
