from calendar import HTMLCalendar
from django import template
from datetime import date, datetime
from itertools import groupby
from django.utils.html import conditional_escape as esc
from workouts.models import *

register = template.Library()


def do_workout_calendar(parser, token):
    try:
        tag_name, year, month, workout_list = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires three arguments" % token.contents.split()[0]

    return WorkoutCalendarNode(year, month, workout_list)


class WorkoutCalendarNode(template.Node):
    def __init__(self, year, month, workout_list):
        try:
            self.year = template.Variable(year)
            self.month = template.Variable(month)
            self.workout_list = template.Variable(workout_list)
        except ValueError:
            raise template.TemplateSyntaxError

    def render(self, context):
        try:
            my_workout_list = self.workout_list.resolve(context)
            my_year = self.year.resolve(context)
            my_month = self.month.resolve(context)
            cal = WorkoutCalendar(my_workout_list)

            return cal.formatmonth(int(my_year), int(my_month))
        except ValueError:
            return
        except template.VariableDoesNotExist:
            return


class WorkoutCalendar(HTMLCalendar):
    def __init__(self, workouts):
        super(WorkoutCalendar, self).__init__()
        self.workouts = self.group_by_day(workouts)

    def formatday(self, day, weekday):
        if day != 0:
            cssclass = self.cssclasses[weekday]
            today = False
            if date.today() == date(self.year, self.month, day):
                today = True
                cssclass += ' today'
            if day in self.workouts:
                if not today:
                    cssclass += ' filled'

                body = [
                    '<span class="workoutAdd"><a href="workouts/edit/%s/%s/%s" title="Edit Workout">'
                    % (self.month, day, self.year),
                    '<span class="glyphicon glyphicon-pencil"></span>',
                    '</a></span>',
                    '<ul class="calendar_day">',
                ]

                for workout in self.workouts[day]:
                    workout_sets = WorkoutSets.objects.filter(workout=workout)
                    num_sets = len(workout_sets)
                    fmt = '%H:%M:%S'
                    time_started = workout.time_started.strftime(fmt)
                    time_ended = workout.time_ended.strftime(fmt)
                    workout_muscles = []
                    for workout_set in workout_sets:
                        muscle_name = workout_set.exercise.muscle.title()
                        if muscle_name not in workout_muscles:
                            workout_muscles.append(muscle_name)
                    muscles = ', '.join(workout_muscles)
                    duration = (datetime.strptime(time_ended, fmt) - datetime.strptime(time_started, fmt)).seconds
                    m, s = divmod(duration, 60)
                    h, m = divmod(m, 60)
                    body.append('<li>')
                    if muscles:
                        body.append('<a href="workouts/%d">' % workout.id)  # workout.get_absolute_url())
                        body.append(esc(muscles))
                        body.append('</a>')
                    body.append('<a href="workouts/set/add/%s" title="Add Set">' % workout.id)
                    body.append('&nbsp;<span class="glyphicon glyphicon-plus"></span>')
                    body.append('</a>')
                    body.append('</li>')
                    body.append('<li>')
                    if h or m:
                        body.append('(')
                    if h:
                        body.append('%dh' % h)
                    if m:
                        body.append('%dm' % m)
                    if h or m:
                        body.append(') ')
                    if num_sets:
                        body.append('(%s sets)' % num_sets)
                    body.append('</li>')
                    if workout.notes:
                        body.append('<li>')
                        body.append('<span class="notes">%s</span>' % workout.notes)
                        body.append('</li>')

                body.append('</ul>')

                return self.day_cell(cssclass, '<span class="dayNumber">%d</span> %s' % (day, ''.join(body)))

            return self.day_cell(
                cssclass,
                '<span class="dayNumberNoWorkouts">%d</span>'
                '<span class="workoutAdd"><a href="workouts/add/%s/%s/%s" title="Add Workout">'
                '<span class="glyphicon glyphicon-plus"></span>'
                '</a></span>'
                % (day, self.month, day, self.year)
            )

        return self.day_cell('noday', '&nbsp;')

    def formatmonth(self, year, month):
        self.year, self.month = year, month

        return super(WorkoutCalendar, self).formatmonth(year, month)

    def group_by_day(self, workouts):
        field = lambda workout: workout.time_started.day

        return dict(
            [(day, list(items)) for day, items in groupby(workouts, field)]
        )

    def day_cell(self, cssclass, body):
        return '<td class="%s">%s</td>' % (cssclass, body)

register.tag("workout_calendar", do_workout_calendar)
