highcharts_json = {
    'chart': {
        'type': 'column',
    },
    'title': {
        'text': '',
    },
    'subtitle': {
        'text': '',
    },
    'xAxis': {
        'type': 'category',
        'labels': {
            'rotation': -45,
            'style': {
                'fontSize': '13px',
                'fontFamily': 'Verdana, sans-serif',
            },
        },
    },
    'yAxis': {
        'min': 0,
        'title': {
            'text': 'Sets',
        },
    },
    'legend': {
    },
    'tooltip': {
        'pointFormat': '',
    },
    'series': [{
        'name': '',
        'data': [],
    }],
    'dataLabels': {
        'rotation': -90,
        'color': '#FFFFFF',
        'align': 'right',
        'format': '{point.y:.0f}',
        'y': 10,
        'style': {
            'fontSize': '13px',
            'fontFamily': 'Verdana, sans-serif',
        }
    }
}
