from datetime import date


def named_month(month_number):
    return date(1900, month_number, 1).strftime("%B")
