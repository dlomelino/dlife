from django.shortcuts import render


def home(request):
    context = {}

    return render(request, 'life/index.html', context)
